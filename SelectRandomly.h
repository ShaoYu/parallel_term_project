#ifndef SELECTRANDOMLY_H
#define SELECTRANDOMLY_H

#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <memory.h>
#include <random>
#include <iterator>

// /* C++ Random Vector Usage */
// template<typename Iter, typename RandomGenerator>
// Iter select_randomly(Iter start, Iter end, RandomGenerator& g);

// template<typename Iter>
// Iter select_randomly(Iter start, Iter end);
/* C++ Random Vector Usage */
template<typename Iter, typename RandomGenerator>
Iter select_randomly(Iter start, Iter end, RandomGenerator& g) {
    std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
    std::advance(start, dis(g));
    return start;
}

template<typename Iter>
Iter select_randomly(Iter start, Iter end) {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    return select_randomly(start, end, gen);
}
#endif