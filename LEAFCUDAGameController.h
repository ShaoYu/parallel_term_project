#ifndef LEAFCUDAMCTSGAMECONTROLLER_H
#define LEAFCUDAMCTSGAMECONTROLLER_H

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>
#include <stdlib.h>
#include <stdio.h>
#include "GameController.h"
#include "MCTSNode.h"
#include "CUDAsimulation.h"
#define NUM_STIMULATION 256
#define NUM_ITERATION 64

class LEAFCUDAMCTSGameController: public GameController {
    /*Game controller that uses MCTS to determine the next move.
    This is the class which implements the Monte Carlo Tree Search algorithm.
    It builds a game tree of MCTSNodes and samples the game space until a set
    time has elapsed.
    */
public:
    MCTSNode* root_node;

    LEAFCUDAMCTSGameController() {
    }

    MCTSNode* select() {
        MCTSNode* node = this->root_node;
        // Descend until we find a node that has pending moves, or is terminal
        while (node->pending_moves.empty() && !node->children.empty()) {
            node = node->select_child_ucb();
        }
        return node;
    }

    MCTSNode* expand(MCTSNode* node) {
        int move = *select_randomly(node->pending_moves.begin(), node->pending_moves.end()); // Random pick one move to expand
        return node->expand_move(move);
    }
        
    void update(MCTSNode* node, double result) {
        while (node != NULL) { // Update until root
            node->plays += 1;
            node->score += node->get_score(result);
            node = node->parent;
        }
    }

    int get_next_move(AIGameState state, double time_allowed=1.0) {
        this->root_node = new MCTSNode(state, NULL, -1); // New start root
        int iterations = 0;
        double start_time = (double)std::clock() / CLOCKS_PER_SEC;
        curandState *d_state;
        cudaMalloc(&d_state, NUM_STIMULATION * sizeof(curandState));
        int block_size = NUM_STIMULATION;
        int* dev_board;
        double* dev_results;
        int board_width = state.board_width;
        cudaMalloc((void**)&dev_board, sizeof(int) * board_width * board_width);
        cudaMalloc((void**)&dev_results, sizeof(double) * NUM_STIMULATION);
        while (iterations < NUM_ITERATION) {
            MCTSNode* node = this->select();

            if (!node->pending_moves.empty())
                node = this->expand(node);

            double results[NUM_STIMULATION];

            int next_turn_player = node->state.next_turn_player;

            cudaMemcpy(dev_board, node->state.board, sizeof(int) * board_width * board_width, cudaMemcpyHostToDevice);
            
            cuda_simulate<<<1, block_size>>>((int)NUM_STIMULATION, dev_board, board_width, next_turn_player, dev_results, d_state);

            cudaMemcpy(results, dev_results, sizeof(double) * NUM_STIMULATION, cudaMemcpyDeviceToHost);

            double result = 0.0;
            for (int i = 0; i < NUM_STIMULATION; i++) {
                result += results[i];
            }
            this->update(node, result/NUM_STIMULATION);
            
            iterations += 1;
        }
        cudaFree(d_state);
        cudaFree(dev_board);
        cudaFree(dev_results);

        int move = (*std::max_element(this->root_node->children.begin(), this->root_node->children.end(), [](MCTSNode* a, MCTSNode* b) { return a->plays < b->plays; }))->move;
        delete root_node;
        return move;
    }
};
#endif