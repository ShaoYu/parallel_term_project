#ifndef LEAFMCTSGAMECONTROLLER_H
#define LEAFMCTSGAMECONTROLLER_H

#include "GameController.h"
#include "MCTSNode.h"
#define NUM_THREADS 2

class LEAFMCTSGameController: public GameController {
    /*Game controller that uses MCTS to determine the next move.
    This is the class which implements the Monte Carlo Tree Search algorithm.
    It builds a game tree of MCTSNodes and samples the game space until a set
    time has elapsed.
    */
public:
    MCTSNode* root_node;

    LEAFMCTSGameController() {
    }

    MCTSNode* select() {
        MCTSNode* node = this->root_node;
        // Descend until we find a node that has pending moves, or is terminal
        while (node->pending_moves.empty() && !node->children.empty()) {
            node = node->select_child_ucb();
        }
        return node;
    }

    MCTSNode* expand(MCTSNode* node) {
        int move = *select_randomly(node->pending_moves.begin(), node->pending_moves.end()); // Random pick one move to expand
        return node->expand_move(move);
    }

    double simulate(AIGameState state, int max_iterations=1000) {
        auto state_copy = AIGameState(state); // Deep copy state for simulation
        int move = state_copy.get_random_move();
        while (move != -1) {
            state_copy.play_move(move);
            move = state_copy.get_random_move();
            max_iterations -= 1;
            if (max_iterations <= 0)
                return 0.5;
        }
        return state_copy.game_result();
    }
        
    void update(MCTSNode* node, double result) {
        while (node != NULL) { // Update until root
            node->plays += 1;
            node->score += node->get_score(result);
            node = node->parent;
        }
    }

    int get_next_move(AIGameState state, double time_allowed=1.0) {
        this->root_node = new MCTSNode(state, NULL, -1); // New start root
        int iterations = 0;
        double start_time = (double)std::clock() / CLOCKS_PER_SEC;
        // while ((double)std::clock() / CLOCKS_PER_SEC < start_time + time_allowed) { // Four step in MCTS
        while (iterations < 64) {
            MCTSNode* node = this->select();

            if (!node->pending_moves.empty())
                node = this->expand(node);

            double result = 0.0;
            omp_set_num_threads(NUM_THREADS);
            # pragma omp parallel reduction (+:result)
            result += this->simulate(node->state);
            # pragma omp critical
            result /= NUM_THREADS;

            this->update(node, result);
            
            iterations += 1;
        }
        int move = (*std::max_element(this->root_node->children.begin(), this->root_node->children.end(), [](MCTSNode* a, MCTSNode* b) { return a->plays < b->plays; }))->move;
        delete root_node;       
        return move;
    }
};
#endif