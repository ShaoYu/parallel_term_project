#ifndef HYBRIDTREEGameController_H
#define HYBRIDTREEGameController_H

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>
#include <stdlib.h>
#include <stdio.h>
#include "GameController.h"
#include "MCTSNode.h"
#include "CUDAsimulation.h"
#define NUM_STIMULATION 128

class HYBRIDTREEGameController: public GameController {
    /*Game controller that uses MCTS to determine the next move.
    This is the class which implements the Monte Carlo Tree Search algorithm.
    It builds a game tree of MCTSNodes and samples the game space until a set
    time has elapsed.
    */
public:
    MCTSNode* root_node;

    HYBRIDTREEGameController() {
    }

    MCTSNode* select(MCTSNode* node) {
        // Descend until we find a node that has pending moves, or is terminal
        while (node->pending_moves.empty() && !node->children.empty()) {
            node = node->select_child_ucb();
        }
        return node;
    }

    MCTSNode* expand(MCTSNode* node) {
        int move = *select_randomly(node->pending_moves.begin(), node->pending_moves.end()); // Random pick one move to expand
        return node->expand_move(move);
    }
        
    void update(MCTSNode* node, double result) {
        while (node != NULL) { // Update until root
            node->plays += 1;
            node->score += node->get_score(result);
            node = node->parent;
        }
    }

    int get_next_move(AIGameState state, double time_allowed=1.0) {
        this->root_node = new MCTSNode(state, NULL, -1);

        curandState *d_state;
        cudaMalloc(&d_state, NUM_STIMULATION * sizeof(curandState));

        int block_size = NUM_STIMULATION;
        int* dev_board;
        double* dev_results;
        int board_width = state.board_width;
        cudaMalloc((void**)&dev_board, sizeof(int) * board_width * board_width);
        cudaMalloc((void**)&dev_results, sizeof(double) * NUM_STIMULATION);

        #pragma omp parallel for
        for (int iterations = 0; iterations < 128; iterations += 1) {
            MCTSNode* node = this->root_node;
            
            #pragma omp critical
            {
                // Descend until we find a node that has pending moves, or is terminal
                while (node->pending_moves.empty() && !node->children.empty()) {
                    node = node->select_child_ucb();
                }

                if (!node->pending_moves.empty()) {
                    int move = *select_randomly(node->pending_moves.begin(), node->pending_moves.end());
                    node = node->expand_move(move);
                }
            }

            double results[NUM_STIMULATION];
            
            int next_turn_player = node->state.next_turn_player;

            cudaMemcpy(dev_board, node->state.board, sizeof(int) * board_width * board_width, cudaMemcpyHostToDevice);
            
            cuda_simulate<<<1, block_size>>>((int)NUM_STIMULATION, dev_board, board_width, next_turn_player, dev_results, d_state);

            cudaMemcpy(results, dev_results, sizeof(double) * NUM_STIMULATION, cudaMemcpyDeviceToHost);

            double result = 0.0;
            for (int i = 0; i < NUM_STIMULATION; i++) {
                result += results[i];
            }

            #pragma omp critical
            {
                while (node != NULL) { // Update until root
                    node->plays += 1;
                    node->score += node->get_score(result);
                    node = node->parent;
                }
            }
        }

        cudaFree(dev_board);
        cudaFree(dev_results);
        cudaFree(d_state);

        int move = (*std::max_element(this->root_node->children.begin(), this->root_node->children.end(), [](MCTSNode* a, MCTSNode* b) { return a->plays < b->plays; }))->move;
        delete this->root_node;
        return move;
    }
};
#endif