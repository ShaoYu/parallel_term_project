## Compilation and Execution
- Compile : PATH_TO_CUDA_VERSION/nvcc -Xcompiler -fopenmp MCTS.cu
- Execute : OMP_NUM_THREADS=$(thread) ./a.out $(board_size)

## Serial AI (Game controller)
- Random player : RandomGameController.h
- Monte Carlo Tree Search : MCTSGameController.h
- Serial Multi-Simulation : MCTSMulGameController.h

## Parallel AI (Game controller)
**Related work in CPU**
- Leaf parallelism : LEAFMCTSGameController.h
- Root parallelism : ROOTMCTSGameController.h
- Tree parallelism : TREEMCTSGameController.h

**Multi-simulation in GPU**
- Leaf parallelism : LEAFCUDAGameController.h
- Hybrid Root : HYBRIDROOTGameController.h
- Hybrid Tree : HYBRIDTREEGameController.h

## Project Report
<object data="report_12.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="report_12.pdf">
        <p> <a href="report_12.pdf">Download PDF</a></p>
    </embed>
</object>