#ifndef TREEMCTSGAMECONTROLLER_H
#define TREEMCTSGAMECONTROLLER_H

#include "GameController.h"
#include "MCTSNode.h"

class TREEMCTSGameController: public GameController {
    /*Game controller that uses MCTS to determine the next move.
    This is the class which implements the Monte Carlo Tree Search algorithm.
    It builds a game tree of MCTSNodes and samples the game space until a set
    time has elapsed.
    */
public:
    MCTSNode* root_node;

    TREEMCTSGameController() {
    }

    int get_next_move(AIGameState state, double time_allowed=1.0) {
        this->root_node = new MCTSNode(state, NULL, -1); // New start root
        double start_time = (double)std::clock() / CLOCKS_PER_SEC;
        // while ((double)std::clock() / CLOCKS_PER_SEC < start_time + time_allowed) { // Four step in MCTS
        #pragma omp parallel for
        for (int iterations = 0; iterations < 64; iterations += 1) {
            MCTSNode* node = this->root_node;
            
            #pragma omp critical
            {
                // Descend until we find a node that has pending moves, or is terminal
                while (node->pending_moves.empty() && !node->children.empty()) {
                    node = node->select_child_ucb();
                }

                if (!node->pending_moves.empty()) {
                    int move = *select_randomly(node->pending_moves.begin(), node->pending_moves.end());
                    node = node->expand_move(move);
                }
            }
            auto state_copy = AIGameState(node->state); // Deep copy state for simulation
            int move = state_copy.get_random_move();
            int max_iterations = 1000;
            double result;
            while (move != -1) {
                state_copy.play_move(move);
                move = state_copy.get_random_move();
                max_iterations -= 1;
                if (max_iterations <= 0)
                    break;
            }
            if (max_iterations <= 0)
                result = 0.5;
            else
                result = state_copy.game_result();

            #pragma omp critical
            {
                while (node != NULL) { // Update until root
                    node->plays += 1;
                    node->score += node->get_score(result);
                    node = node->parent;
                }
            }
        }
        int move = (*std::max_element(this->root_node->children.begin(), this->root_node->children.end(), [](MCTSNode* a, MCTSNode* b) { return a->plays < b->plays; }))->move;
        delete root_node;       
        return move;
    }
};
#endif