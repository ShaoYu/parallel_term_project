#ifndef AIGAMESTATE_H
#define AIGAMESTATE_H

#include "GameState.h"

class AIGameState: public GameState {
    /* AIGameState.
    The implementation of AI term project.
    */
public:
    int next_turn_player;
    int board_width;
    int* board;

    AIGameState(int board_width=4, int next_turn_player=0) {
        if (board_width <= 3 || board_width % 2 != 0) // Check legal boardwith
            exit(1);
        this->next_turn_player = next_turn_player; // Current turn player
        this->board_width = board_width;
        this->board = new int[board_width * board_width];
        for (int i = 0; i < board_width; i += 2) { // Initial board by crossing 0 and 1 
            for (int j = 0; j < board_width; j += 2)
                this->board[i * this->board_width + j] = 0;
            for (int j = 1; j < board_width; j += 2)
                this->board[i * this->board_width + j] = 1;
        }
        for (int i = 1; i < board_width; i += 2) {
            for (int j = 0; j < board_width; j += 2)
                this->board[i * this->board_width + j] = 1;
            for (int j = 1; j < board_width; j += 2)
                this->board[i * this->board_width + j] = 0;
        }
    }

    AIGameState(const AIGameState& state) { // Implement for deep copy
        this->next_turn_player = state.next_turn_player;
        this->board_width = state.board_width;
        this->board = new int[this->board_width * this->board_width];
        memcpy(this->board, state.board, sizeof(int) * board_width * board_width);
    }

    bool check_in_board(int* index) { // Check index in board or not
        return index[0] >= 0 && index[0] < this->board_width && index[1] >= 0 && index[1] < this->board_width;
    }

    double game_result() { // Return game result
        bool in_board[2] = {false};
        for (int i = 0; i < this->board_width; i++) {
            for (int j = 0; j < this->board_width; j++){
                if (this->board[i * this->board_width + j] == 0)
                    in_board[0] = true;
                if (this->board[i * this->board_width + j] == 1)
                    in_board[1] = true;
            }
        }
        if (in_board[0] && in_board[1]) // Game is not finished
            return -1.0;
        if (in_board[0]) // 0 lose
            return 1.0;
        if (in_board[1]) // 0 win
            return 0.0;
        return 0.5; // Draw
    }

    std::vector<int> get_moves() { // Get all possible moves in this state
        std::vector<int> moves;
        for (int i = 0; i < this->board_width; i++) {
            for (int j = 0; j < this->board_width; j++){
                if (this->board[i * this->board_width + j] == this->next_turn_player)
                    moves.push_back(i * this->board_width + j);
            }
        }
        return moves;
    }

    int get_random_move() {
        std::vector<int> moves = this->get_moves();
        if (moves.empty() || this->game_result() != -1.0) {
            return -1;
        }
        int r = *select_randomly(moves.begin(), moves.end());
        return r;
    }

    void play_move(int move) {
        // Play move to -1
        this->board[move] = -1;

        // Find all neighbors
        int udlr[4][2];
        udlr[0][0] = move / this->board_width - 1;
        udlr[0][1] = move % this->board_width;
        udlr[1][0] = move / this->board_width + 1;
        udlr[1][1] = move % this->board_width;
        udlr[2][0] = move / this->board_width;
        udlr[2][1] = move % this->board_width - 1;
        udlr[3][0] = move / this->board_width;
        udlr[3][1] = move % this->board_width + 1;

        // Check side effect on up, down, left, right neighbors
        for (int i = 0; i < 4; i++) {
            if (check_in_board(udlr[i])) { // Check neighbor is in the board
                int tmp[2];
                // Check up of neighbor
                tmp[0] = udlr[i][0] - 1;
                tmp[1] = udlr[i][1];
                if (check_in_board(tmp)) { // Check up of neighbor is in the board
                    if (this->board[tmp[0] * this->board_width + tmp[1]] == -1 && (tmp[0] * this->board_width + tmp[1]) != move) { // Check side effect
                        this->board[udlr[i][0] * this->board_width + udlr[i][1]] = -1; // If side effect, remove neighbor
                    }
                }
                // Check down of neighbor
                tmp[0] = udlr[i][0] + 1;
                tmp[1] = udlr[i][1];
                if (check_in_board(tmp)) {
                    if (this->board[tmp[0] * this->board_width + tmp[1]] == -1 && (tmp[0] * this->board_width + tmp[1]) != move) { // Check side effect
                        this->board[udlr[i][0] * this->board_width + udlr[i][1]] = -1; // If side effect, remove neighbor
                    }
                }
                // Check left of neighbor
                tmp[0] = udlr[i][0];
                tmp[1] = udlr[i][1] - 1;
                if (check_in_board(tmp)) {
                    if (this->board[tmp[0] * this->board_width + tmp[1]] == -1 && (tmp[0] * this->board_width + tmp[1]) != move) { // Check side effect
                        this->board[udlr[i][0] * this->board_width + udlr[i][1]] = -1; // If side effect, remove neighbor
                    }
                }
                // Check right of neighbor
                tmp[0] = udlr[i][0];
                tmp[1] = udlr[i][1] + 1;
                if (check_in_board(tmp)) {
                    if (this->board[tmp[0] * this->board_width + tmp[1]] == -1 && (tmp[0] * this->board_width + tmp[1]) != move) { // Check side effect
                        this->board[udlr[i][0] * this->board_width + udlr[i][1]] = -1; // If side effect, remove neighbor
                    }
                }
            }
        }
        // In turn player
        this->next_turn_player = 1 - this->next_turn_player;
    }

    std::string to_string() { // Board to string
        std::string s;
        for (int i = 0; i < this->board_width; i++) {
            for (int j = 0; j < this->board_width; j++) {
                if (this->board[i * this->board_width + j] == -1)
                    s += "X ";
                else if (this->board[i * this->board_width + j] == 0)
                    s += "B ";
                else if (this->board[i * this->board_width + j] == 1)
                    s += "R ";
            }
            s += '\n';
        }
        return s;
    }
};
#endif