#ifndef RANDOMGAMECONTROLLER_H
#define RANDOMGAMECONTROLLER_H

#include "GameController.h"

class RandomGameController: public GameController {
    /*Random Game Controller.
    This game controller will play any game by simply selecting moves at random.
    It serves as a benchmark for the performance of the MCTSGameController.*/
public:

    RandomGameController() {
    }

    int get_next_move(AIGameState state) {
        std::vector<int> moves = state.get_moves();
        int r = *select_randomly(moves.begin(), moves.end()); // Random pick one from all possible move
        return r;
    }
};
#endif