#ifndef CUDASIMULATION_H
#define CUDASIMULATION_H

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>
#include <stdlib.h>
#include <stdio.h>
#include "GameController.h"
#include "MCTSNode.h"

__device__ 
bool cuda_check_in_board(int* index, int board_width) { // Check index in board or not
    return index[0] >= 0 && index[0] < board_width && index[1] >= 0 && index[1] < board_width;
}

__device__
int cuda_get_random_move(int* board, int board_width, int next_turn_player, curandState *state, int idx) {
    bool in_board[2] = {false};
    int cnt = 0;
    int board_square = board_width * board_width;
    int moves[256];
    for (int i = 0; i < board_square; i++) {
        if (board[i]==next_turn_player) {
            moves[cnt++] = i;
        }
        if (board[i] != -1) {
            in_board[board[i]] = true;
        }
    }
    if (in_board[0] && !in_board[1])
        return -2;
    if (in_board[1] && !in_board[0])
        return -3;
    if (cnt==0)
        return -1;
    int r = curand(&state[0]) % cnt;
    return moves[r];
}

__global__
void cuda_simulate(int simulations, int* d_board, int board_width, int next_turn_player, double* dev_results, curandState *state, int max_iterations=1000) {
    int idx = threadIdx.x;
    if (idx==0)
        curand_init(1337, 0, 0, &state[0]);
    int board[256];
    int board_square = board_width * board_width;
    for (int i = 0; i < board_square; i++) {
        board[i] = d_board[i]; // initial local board
    }
    int move = cuda_get_random_move(board, board_width, next_turn_player, state, idx);
    while (move >= 0) {
        /*play move*/
        board[move] = -1;
        
        // Find all neighbors
        int udlr[4][2];
        udlr[0][0] = move / board_width - 1;
        udlr[0][1] = move % board_width;
        udlr[1][0] = move / board_width + 1;
        udlr[1][1] = move % board_width;
        udlr[2][0] = move / board_width;
        udlr[2][1] = move % board_width - 1;
        udlr[3][0] = move / board_width;
        udlr[3][1] = move % board_width + 1;
        // Check side effect on up, down, left, right neighbors
        for (int i = 0; i < 4; i++) {
            if (cuda_check_in_board(udlr[i], board_width)) { // Check neighbor is in the board
                int tmp[2];
                // Check up of neighbor
                tmp[0] = udlr[i][0] - 1;
                tmp[1] = udlr[i][1];
                if (cuda_check_in_board(tmp, board_width)) { // Check up of neighbor is in the board
                    if (board[tmp[0] * board_width + tmp[1]] == -1 && (tmp[0] * board_width + tmp[1]) != move) { // Check side effect
                        board[udlr[i][0] * board_width + udlr[i][1]] = -1; // If side effect, remove neighbor
                    }
                }
                // Check down of neighbor
                tmp[0] = udlr[i][0] + 1;
                tmp[1] = udlr[i][1];
                if (cuda_check_in_board(tmp, board_width)) {
                    if (board[tmp[0] * board_width + tmp[1]] == -1 && (tmp[0] * board_width + tmp[1]) != move) { // Check side effect
                        board[udlr[i][0] * board_width + udlr[i][1]] = -1; // If side effect, remove neighbor
                    }
                }
                // Check left of neighbor
                tmp[0] = udlr[i][0];
                tmp[1] = udlr[i][1] - 1;
                if (cuda_check_in_board(tmp, board_width)) {
                    if (board[tmp[0] * board_width + tmp[1]] == -1 && (tmp[0] * board_width + tmp[1]) != move) { // Check side effect
                        board[udlr[i][0] * board_width + udlr[i][1]] = -1; // If side effect, remove neighbor
                    }
                }
                // Check right of neighbor
                tmp[0] = udlr[i][0];
                tmp[1] = udlr[i][1] + 1;
                if (cuda_check_in_board(tmp, board_width)) {
                    if (board[tmp[0] * board_width + tmp[1]] == -1 && (tmp[0] * board_width + tmp[1]) != move) { // Check side effect
                        board[udlr[i][0] * board_width + udlr[i][1]] = -1; // If side effect, remove neighbor
                    }
                }
            }
        }
        // In turn player
        next_turn_player = 1 - next_turn_player;
        /*get random move*/
        move = cuda_get_random_move(board, board_width, next_turn_player, state, idx);
    }
    if (move==-2) { // 0 lose
        dev_results[idx] = 1.0;
        return;
    }
    if (move==-3) { // 0 win
        dev_results[idx] = 0.0;
        return;
    }
    dev_results[idx] = 0.5; // Draw
    return;
}
#endif