#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include "AIGameState.h"

class GameController {
    /*Game Controller.
    A generic interface that all game controllers must abide by so that they
    can be put on trial against one another. Each game is conducted between
    a pair of concrete GameController instances representing the two players.*/
public:

    GameController() {
    }

    int get_next_move(AIGameState state) {
        return 0;
    }
};
#endif