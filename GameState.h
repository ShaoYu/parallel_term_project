#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <vector>
#include "SelectRandomly.h"

using namespace std;

class GameState {
    /* GameState generic base class.
    This class represents the interface that other game classes must conform to.
    It assumes a two-player game. The players are numbered 0 and 1, with the
    first move always going to player 0. Game results are expected to be in the
    range [0.0, 1.0] representing whether player 0 or 1 is the victor.
    In the case of a draw the result should be 0.5.
    */
public:
    int next_turn_player; 
    GameState() {
        this->next_turn_player = 0;
    }

    double game_result() {
        return -1.0;
    }

    std::vector<int> get_moves() {
        std::vector<int> moves;
        return moves;
    }

    int get_random_move() {
        std::vector<int> moves = this->get_moves();
        if (moves.empty()) {
            return -1;
        }
        int r = *select_randomly(moves.begin(), moves.end());
        return r;
    }

    void play_move(int move) {
    }
};
#endif