#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <memory.h>
#include <random>
#include <iterator>
#include <algorithm>
#include <omp.h>

#include "GameState.h"
#include "GameController.h"
#include "RandomGameController.h"
#include "MCTSNode.h"
#include "MCTSGameController.h"
#include "MCTSMulGameController.h"
#include "ROOTMCTSGameController.h"
#include "LEAFMCTSGameController.h"
#include "TREEMCTSGameController.h"

#include "LEAFCUDAGameController.h"
#include "HYBRIDROOTGameController.h"
#include "HYBRIDTREEGameController.h"

// #define P0RANDOM
// #define P0MCTS
// #define P0MCTSMUL
// #define P0LEAF
// #define P0ROOT
// #define P0TREE
// #define P0LEAFCUDA
// #define P0HYBRIDROOT
#define P0HYBRIDTREE

// #define P1RANDOM
// #define P1MCTSMUL
// #define P1MCTS
// #define P1LEAF
// #define P1ROOT
// #define P1TREE
#define P1LEAFCUDA
// #define P1HYBRIDROOT
// #define P1HYBRIDTREE

using namespace std;

void main_program(int board_width=8)
{
    #ifdef P0RANDOM
    RandomGameController player0 = RandomGameController();
    #endif

    #ifdef P0MCTS
    MCTSGameController player0 = MCTSGameController();
    #endif

    #ifdef P0MCTSMUL
    MCTSMulGameController player0 = MCTSMulGameController();
    #endif

    #ifdef P0ROOT
    ROOTMCTSGameController player0 = ROOTMCTSGameController();
    #endif

    #ifdef P0LEAF
    LEAFMCTSGameController player0 = LEAFMCTSGameController();
    #endif

    #ifdef P0TREE
    TREEMCTSGameController player0 = TREEMCTSGameController();
    #endif

    #ifdef P0LEAFCUDA
    LEAFCUDAMCTSGameController player0 = LEAFCUDAMCTSGameController();
    #endif

    #ifdef P0HYBRIDROOT
    HYBRIDROOTGameController player0 = HYBRIDROOTGameController();
    #endif

    #ifdef P0HYBRIDTREE
    HYBRIDTREEGameController player0 = HYBRIDTREEGameController();
    #endif

    #ifdef P1RANDOM
    RandomGameController player1 = RandomGameController();
    #endif

    #ifdef P1MCTS
    MCTSMulGameController player1 = MCTSMulGameController();
    #endif

    #ifdef P1MCTSMUL
    MCTSMulGameController player0 = MCTSMulGameController();
    #endif

    #ifdef P1ROOT
    ROOTMCTSGameController player1 = ROOTMCTSGameController();
    #endif

    #ifdef P1LEAF
    LEAFMCTSGameController player1 = LEAFMCTSGameController();
    #endif

    #ifdef P1TREE
    TREEMCTSGameController player1 = TREEMCTSGameController();
    #endif

    #ifdef P1LEAFCUDA
    LEAFCUDAMCTSGameController player1 = LEAFCUDAMCTSGameController();
    #endif

    #ifdef P1HYBRIDROOT
    HYBRIDROOTGameController player1 = HYBRIDROOTGameController();
    #endif

    #ifdef P1HYBRIDTREE
    HYBRIDTREEGameController player1 = HYBRIDTREEGameController();
    #endif

    int wins[2] = {0};

	for (int i = 0; i < 10; i++) {
        auto game_state = AIGameState(board_width);
        int player = 0;
        while (game_state.game_result() == -1) {
            // cout << endl << "State: " << endl << game_state.to_string() << endl;
            int next_move = -1;
            if (player==0)
                next_move = player0.get_next_move(game_state);
            // else if (player==1)
            //     next_move = player1.get_next_move(game_state);
            game_state.play_move(next_move);
            player = 1 - player;
        }
        return;
        // cout << endl << "Final state: " << endl << game_state.to_string() << endl;
        if (game_state.game_result() == 0.0) {
		    cout << "Player 0 wins!" << endl;
            wins[0] += 1;
        }
        else if (game_state.game_result() == 1.0) {
            cout << "Player 1 wins!" << endl;
            wins[1] += 1;
        }
        else {
            cout << "Nobody wins!" << endl;
        }
    }
    for (int i = 0; i < 10; i++) {
        auto game_state = AIGameState(board_width);
        int player = 0;
        while (game_state.game_result() == -1) {
            // cout << endl << "State: " << endl << game_state.to_string() << endl;
            int next_move = -1;
            if (player==0)
                next_move = player1.get_next_move(game_state);
            else if (player==1)
                next_move = player0.get_next_move(game_state);
            game_state.play_move(next_move);
            player = 1 - player;
        }
        // cout << endl << "Final state: " << endl << game_state.to_string() << endl;
        if (game_state.game_result() == 0.0) {
		    cout << "Player 1 wins!" << endl;
            wins[1] += 1;
        }
        else if (game_state.game_result() == 1.0) {
            cout << "Player 0 wins!" << endl;
            wins[0] += 1;
        }
        else {
            cout << "Nobody wins!" << endl;
        }
    }
    cout << "0 wins:" << wins[0] << endl << "1 wins:" << wins[1] << endl;
};

int main(int argc, char *argv[]) {
    if (argc == 2)
        main_program(atoi(argv[1]));
    else
        main_program(16);
    return 0;
}
