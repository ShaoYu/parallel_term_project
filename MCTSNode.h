#ifndef MCTSNODE_H
#define MCTSNODE_H

#include <algorithm>
#include "AIGameState.h"

class MCTSNode {
    /*Monte Carlo Tree Node.
    Each node encapsulates a particular game state, the moves that
    are possible from that state and the strategic information accumulated
    by the tree search as it progressively samples the game space.
    */
public:
    MCTSNode* parent;
    int move;
    AIGameState state;
    int plays; // Visit count
    double score; // Play result
    double UCT_score; // UCT_socre
    std::vector<MCTSNode*> children;
    std::vector<int> pending_moves;

    MCTSNode(AIGameState state, MCTSNode* parent, int move) {
        this->parent = parent;
        this->move = move;
        this->state = state;
        // auto state_copy = AIGameState(state);
        // this->state = state_copy;
        this->plays = 0;
        this->score = 0.0;
        this->UCT_score = 0.0;
        this->pending_moves = state.get_moves(); // All possible moves from the state (don't move yet)
    }

    MCTSNode* select_child_ucb() {
        for (auto child: this->children) { // Calculate UCT score for all children
            child->UCT_score = double(child->score) / double(child->plays) + std::sqrt(2.0 * std::log(double(this->plays)) / child->plays);
        }
        return *std::max_element(children.begin(), children.end(), [](MCTSNode* a, MCTSNode* b) { return a->UCT_score < b->UCT_score; }); // Select max UCT score child
    }

    MCTSNode* expand_move(int move) {
        // Get move and remove it from pending_moves
        auto itr = this->pending_moves.begin();
        for (; itr != this->pending_moves.end() && *itr != move; itr++);
        this->pending_moves.erase(itr);
        auto child_state = AIGameState(this->state); // Deep copy state
        child_state.play_move(move);

        // New child node and append it into children. Then return child
        MCTSNode* child = new MCTSNode(child_state, this, move);
        this->children.push_back(child);
        return child;
    }

    double get_score(double result) { // Get the node score
        // if (result == 0.5)
        //     return 0.5;
        // if (double(this->state.next_turn_player) == result)
        //     return 0.0;
        // else
        //     return 1.0;
        if (this->state.next_turn_player == 1) {
            return 1.0 - result;
        }
        else {
            return result;
        }
    }

    ~MCTSNode() {
        for (auto child: children) {
            delete child;
        }
    }
};
#endif